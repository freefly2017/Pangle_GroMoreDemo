# 安卓隐私页面相关插件已开发完成，一键导入Unity工程，不需要在Android Studio中操作。如有问题请加QQ群讨论：436320504 查看最终效果请看我的一个线上的游戏，多谢支持。链接：https://www.taptap.com/app/233281

# 穿山甲GroMoreDemo 兼容Unity2021.2及以上版本  增加iOS支持.

#### 关于打包apk出错的提醒
用unity直接打包里会报一些错误，本人是导出Android工程，从AndroidStudio中打包。

#### 介绍
由于近期大家使用GroMore比较多，发现穿山甲的Demo有些复杂，增加了大家的接入时间成本，用了一天时间简化了一下Demo，官网Demo由于版本低，会造成在Unity打包时报错，一并修改完成，希望能给大家提供一些帮助。使用时只需要把这个包导入工程即可。目前只测试了Android工程。后续有时间会把iOS工程一并验证。

#### 软件架构
由于官网版本为Unity2019环境下开发，用Unity2019版本打包很顺利，我就不再提供2019版本的支持了，大家如果想使用我的广告管理类，只需要在导入Patch的时候只选择广告类(GroMoreSdkManager.cs)就可以了。

#### 安装教程 （针对Unity2020以上版本）
1.  特别提醒：如果需要64位包需要特别注意插件与Unity版本的对应，64位需要在Unity打包配置里设置成IL2CPP。
2.  下载官网的Demo 和 SDK之后导入到Unity工程。
3.  再导入我的GroMorePatch即可。
3.  把以下Scene添加到BuildSetting中，A4Game/Main,A4Game/GroMore,Sample/SampleScene,Main需放在第一个位置上。
4.  修改包名为：com.DefaultCompany.GroMoreSdkPro （可能不是必须的）

#### 使用说明

1.  如果在自己的工程内使用，请参考GroMoreSdkManager类，简化了一下调用。
2.  GroMoreSdkManager类中只提供了激励视频，插页和Banner三种，其他广告类型请参考AdFunctionScript中的方法自行添加。
3.  AppId的修改在UnionApplication.java类中，目录为：MSDK/Scripts/Android/UnionApplication.java
4.  AndroidStudio打包时会报一个混淆的错误，官网Demo少写了class，对应修改为：
    `-keep class com.bytedance.ad.sdk.mediation.AdManager { *; }
    -keep class com.bytedance.ad.sdk.mediation.FeedAdManager { *; }    
    -keep class com.bytedance.ad.sdk.mediation.FeedView { *; }
    -keep class com.bytedance.ad.sdk.mediation.MResource { *; }
    -keep class com.bytedance.ad.sdk.mediation.TToast { *; }
    -keep class com.bytedance.ad.sdk.mediation.UIUtils { *; }
    -keep class com.bytedance.ad.sdk.mediation.VideoOptionUtil { *; }`
5.  如需要帮助请加QQ群：436320504


#### 特别提醒

1.由于官网SDK更新频繁，特提供一个补丁包，只集成了我自己的广告管理类和对官网Bug的一些修复文件。
2.补丁包使用说明：下载最新官网SDK及Demo导入自己的工程，再导入我提供的补丁包就可以了。
3.最新的官网SDK对Banner的一个方法进行了修改，请知悉GetAdRitInfoAdnName();我在广告管理类里已经修改。

#### 如官网SDK有更新请及时通知我，我会给大家测试并提供最新版本。


#### iOS相关支持
1.官网对iOS的配置比较麻烦,可能很多同学还不太适应,集成本插件,只需设置三处即可完成接入.
2.对于聚合哪些SDK的配置:
![输入图片说明](imagesdepend.png)
我只集成了这几个,有需要的可自行增删
```
<?xml version="1.0" encoding="utf-8"?>
<dependencies>
    <iosPods>
        <sources>
            <source>https://github.com/Yodo1Sdk/Yodo1Spec.git</source>
        </sources>
        <iosPod name="Ads-CN" version="4.2.5.3" bitcode="false" minTargetSdk="9.0" />
        <!--    百度SDK    -->
        <iosPod name="BaiduMobAdSDK" version="4.843" bitcode="false" minTargetSdk="9.0" />
        <!--   广点通/优量汇  -->
        <iosPod name="GDTMobSDK" version="4.13.40" bitcode="false" minTargetSdk="9.0" />
        <!--   SigmobAd  -->
        <iosPod name="SigmobAd-iOS" version="3.5.0" bitcode="false" minTargetSdk="9.0" />
        <!--   游可赢  -->
        <iosPod name="KlevinAdSDK" version="2.5.0.230" bitcode="false" minTargetSdk="9.0" />
        <!--   快手  -->
        <iosPod name="KSAdSDK" version="3.3.20" bitcode="false" minTargetSdk="9.0" />
        <!--   Admob  -->
        <iosPod name="Google-Mobile-Ads-SDK" version="8.13.0" bitcode="false" minTargetSdk="9.0" />
    </iosPods>
</dependencies>

```

#### iOS使用说明  插件已经支持iOS,开发环境为2021.3.4f1.

1.导入官网SDK和Demo.

2.导入本插件.

3.修改配置如下:
![输入图片说明](imagespodsetting.png)
![输入图片说明](imagespodsettingstatic.png)

4.导出xcode工程,如果添加了Admob依赖库会更新不到报错,请自行解决.

5.确认导出成功,没有报错.

6.打开xcode工程目录,注意是打开目录,如果是以下结构就没有问题.
![输入图片说明](imagesproject.png)

7.如果不是第6步所示,请进入命令行,进入xcode工程目录执行pod install

8.如果聚合了快手SDK,进行以下设置:(如果没有集成快手,可直接跳到第9步.
![输入图片说明](imagesks.png)
![输入图片说明](imagesks2.png)
![输入图片说明](imagesbitcode.png)

9.打开xcode工程一定要点击下面这个文件,切记!!!!!!
![输入图片说明](imagesproject.png)

10.手机运行即可.

如果您觉得本插件对您有所帮助，也可以请作者喝杯奶茶。感谢您的支持！
![输入图片说明](paycodepaycode.png)